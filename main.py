if __name__ == '__main__':
    from core import Core
    from components import Component, Editor
    from keybindings import keybindings

    editor = Editor(filepath = 'example.py')

    app = Core(
        cntr=[
            {
                'cmp': editor,
            },
            {
                'cmp': Component(),
                'rows': 1
            }
        ],
        actions={
            'Editor.insert': editor.text_area.doc.insert,
            'Editor.insert_space': editor.text_area.doc.insert_space,
            'Editor.indent': editor.text_area.doc.indent,
            'Editor.new_line': editor.text_area.doc.new_line,
            'Editor.delete_left': editor.text_area.doc.delete_left,
            'Editor.delete_right': editor.text_area.doc.delete_right,
            'Editor.delete_left_word': editor.text_area.doc.delete_left_word,
            'Editor.delete_right_word': editor.text_area.doc.delete_right_word,
            'Editor.Cursor.move_up': editor.text_area.doc.move_up,
            'Editor.Cursor.move_down': editor.text_area.doc.move_down,
            'Editor.Cursor.move_left': editor.text_area.doc.move_left,
            'Editor.Cursor.move_right': editor.text_area.doc.move_right,
        },
        keybindings=keybindings
    )

    app.run()