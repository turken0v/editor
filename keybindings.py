from pynput.keyboard import Key
from pynput.keyboard import KeyCode

keybindings = (
    ({Key.ctrl, KeyCode.from_char('q')}, (), 'core.quit'),
    ({Key.space}, (), 'Editor.insert_space'),
    ({Key.tab}, (), 'Editor.indent'),
    ({Key.enter}, (), 'Editor.new_line'),
    ({Key.backspace}, (), 'Editor.delete_left'),
    ({Key.delete}, (), 'Editor.delete_right'),
    ({Key.ctrl_l, Key.backspace}, (), 'Editor.delete_left_word'),
    ({Key.ctrl_l, Key.delete}, (), 'Editor.delete_right_word'),
    ({Key.up}, (), 'Editor.Cursor.move_up'),
    ({Key.down}, (), 'Editor.Cursor.move_down'),
    ({Key.left}, (), 'Editor.Cursor.move_left'),
    ({Key.right}, (), 'Editor.Cursor.move_right'),
)