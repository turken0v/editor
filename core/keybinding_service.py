from pynput.keyboard import Key


class KeybindingService:
    _mods = set()
    _buffer = []


    def __init__(self, actions, keybindings):
        self._actions = actions
        self._kbs = keybindings


    def _call_binding(self, when, callable_cmp:str, **kwargs):
        action = self._actions.get(callable_cmp)
        if callable(action):
            action(**kwargs)


    @staticmethod
    def _is_mod(key):
        if key in set([
            Key.shift, Key.shift_l, Key.shift_r,
            Key.ctrl, Key.ctrl_l, Key.ctrl_r,
            Key.alt, Key.alt_gr, Key.alt_l, Key.alt_r,
            Key.caps_lock,
            Key.cmd, Key.cmd_l, Key.cmd_r
        ]): return True
        return False


    def on_press(self, key):
        if self._is_mod(key):
            self._mods.add(key)
        else:
            current = set([key]) | self._mods

            self._buffer.append(current)


    def on_release(self, key):
        if self._is_mod(key):
            try:
                self._mods.remove(key)
            except:
                self._mods.clear()


    def resolve(self):
        while self._buffer:
            last = self._buffer.pop(0)
            found = False
            for kb in self._kbs:
                if kb[0] == last:
                    self._call_binding(kb[1], kb[2])
                    found = True

            if not found:
                for key in last:
                    if not self._is_mod(key):
                        self._call_binding((), 'Editor.insert', value=key.char)