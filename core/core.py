from components import Component
from .render import Orient, Render
from .keybinding_service import KeybindingService

from blessed import Terminal

from pynput import keyboard


term = Terminal()


def get_app():
	return app


class Core:
	_exit_status = False


	def __init__(self, cntr, actions, keybindings) -> None:
		self.rows = term.height
		self.cols = term.width

		self.cntr = cntr
		self._configure_cmps(self.cntr)

		actions['core.quit'] = self.quit

		self.kbService = KeybindingService(
			actions=actions,
			keybindings=keybindings
		)

		global app
		app = self


	@staticmethod
	def _configure_cmps(cntr) -> None:
		for cmp in cntr:
			cmp_cntr = cmp.get('cntr')
			if cmp_cntr:
				cmp['cmp'] = Component()
				Core._configure_cmps(cmp_cntr)

			cmp_opts = cmp.get('opts')
			if cmp_opts:
				cmp_content = cmp_opts.get('content')
				if cmp_content:
					setattr(cmp['cmp'], 'content', cmp_content)


	@staticmethod
	def _configure_sizes(app_cntr, app_sizes:tuple) -> None:
		def f1(cntr:list, attr:str, orient:int = Orient.h, invert:bool = False) -> tuple:
			"""
			Find every defined attribute in container and return sum or max value depending on orientation
			"""
			value = 0
			num = 0

			if invert:
				orient = Orient.invert(Orient.h if orient is None else orient)

			for cmp in cntr:
				cmp_value = cmp.get(attr)
				if cmp_value:
					if orient == Orient.h:
						value += cmp_value
					elif cmp_value > value:
						value = cmp_value
					setattr(cmp['cmp'], attr, cmp_value)
					num += 1

				cmp_orient = cmp.get('orient', Orient.h)
				_value = 0
				cmp_cntr = cmp.get('cntr')
				if cmp_cntr:
					_value, _ = f1(cmp_cntr, attr, cmp_orient, invert)
				elif hasattr(cmp['cmp'], 'cntr'):
					cmp_cntr = getattr(cmp['cmp'], 'cntr')
					_value, _ = f1(cmp_cntr, attr, cmp_orient, invert)
				value += _value

			return value, num


		def f2(cntr:list, attr:str, size:int, orient:int = Orient.h, invert:bool = False) -> None:
			"""
			Find every undefined attribute in container and set value by the formula depending on orientation
			"""
			dsize, dnum = f1(cntr, attr, orient, invert)
			indent = 0

			if invert:
				orient = Orient.invert(Orient.h if orient is None else orient)

			for cmp in cntr:
				cmp_dsize = 0
				cmp_orient = cmp.get('orient', Orient.h)
				cmp_cntr = cmp.get('cntr')
				if cmp_cntr:
					cmp_dsize, _ = f1(cmp_cntr, attr, cmp_orient, invert)
				elif hasattr(cmp['cmp'], 'cntr'):
					cmp_cntr = getattr(cmp['cmp'], 'cntr')
					cmp_dsize, _ = f1(cmp_cntr, attr, cmp_orient, invert)

				cmp_size = cmp.get(attr)
				if cmp_size is None:
					if orient == Orient.h:
						value = (size - dsize + cmp_dsize) / (len(cntr) - dnum)
						cmp_size = value if value > cmp_dsize else cmp_dsize
					else:
						cmp_size = dsize if dsize > cmp_dsize else cmp_dsize
						cmp_size = size if cmp_dsize == 0 else cmp_dsize
						

					setattr(cmp['cmp'], attr, cmp_size)

				setattr(cmp['cmp'], 'indent_' + attr, indent)
				if orient == Orient.h:
					indent += cmp_size

				if cmp_cntr:
					f2(cmp_cntr, attr, cmp_size, cmp_orient, invert)


		app_rows, app_cols = app_sizes

		f2(app_cntr, 'rows', app_rows, Orient.v, invert=True)

		for cmp in app_cntr:
			f2([cmp], 'cols', app_cols)


	# NOTE remove duplicate funcs
	@staticmethod
	def _call_stack_before(cntr):
		for cmp in cntr:
			cmp_cntr = cmp.get('cntr')
			if cmp_cntr:
				Core._call_stack_before(cmp_cntr)
			elif hasattr(cmp['cmp'], 'cntr'):
				cmp_cntr = getattr(cmp['cmp'], 'cntr')
				Core._call_stack_before(cmp_cntr)

			if hasattr(cmp['cmp'], 'call_stack_before'):
				stack = getattr(cmp['cmp'], 'call_stack_before')
				for func in stack:
					if callable(func):
						func()


	@staticmethod
	def _call_stack_after(cntr):
		for cmp in cntr:
			cmp_cntr = cmp.get('cntr')
			if cmp_cntr:
				Core._call_stack_after(cmp_cntr)
			elif hasattr(cmp['cmp'], 'cntr'):
				cmp_cntr = getattr(cmp['cmp'], 'cntr')
				Core._call_stack_after(cmp_cntr)

			if hasattr(cmp['cmp'], 'call_stack_after'):
				stack = getattr(cmp['cmp'], 'call_stack_after')
				for func in stack:
					if callable(func):
						func()


	def _display(self):
		output = term.move_xy(0, 0) + term.clear + ''.join(Render.by_lines(self.cntr, Orient.v))
		print(output, end='', flush=True)


	def _wait_changes(self):
		term.inkey()


	def run(self):
		listener = keyboard.Listener(
            on_press=self.kbService.on_press,
            on_release=self.kbService.on_release
		)
		listener.start()
		listener.wait()

		with term.raw(), term.fullscreen():
			while not self._exit_status:
				self._call_stack_before(self.cntr)
				self._configure_sizes(self.cntr, (self.rows, self.cols))
				self._display()
				self._call_stack_after(self.cntr)
				self._wait_changes()
				self.kbService.resolve()

		listener.stop()


	def quit(self):
		self._exit_status = True