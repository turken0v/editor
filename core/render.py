class Orient:
	v = 0
	h = 1

	@staticmethod
	def invert(orient):
		return Orient.h if orient == Orient.v else Orient.v


class Render:
	@staticmethod
	def fill_line(cols, content=""):
		return content + " " * int((cols - len(content)))


	@staticmethod
	def format2lines(value, cols):
		output = []
		while True:
			if len(value) > cols:
				output.append(value[:cols])
				value = value[cols:]
			else:
				output.append(Render.fill_line(cols, value))
				break
		return output


	@staticmethod
	def zip(num, *arrs):
		output = []
		iterators = [iter(it) for it in arrs]
		for i in range(num):
			line = ''
			for it in iterators:
				part = ''
				try:
					part = next(it)
				except:
					pass
				line += part
			output.append(line)

		return output


	@staticmethod
	def by_lines(cntr, orient:int = Orient.h):
		output = []
		for cmp in cntr:
			cmp_rows = int(cmp['cmp'].rows)
			cmp_cols = int(cmp['cmp'].cols)
			cmp_lines = cmp['cmp'].get_lines()

			cmp_cntr = cmp.get('cntr')
			# NOTE add orient arg to recursion
			if cmp_cntr:
				cmp_lines = Render.by_lines(cmp_cntr)
			elif hasattr(cmp['cmp'], 'cntr'):
				cmp_cntr = getattr(cmp['cmp'], 'cntr')
				cmp_lines = Render.by_lines(cmp_cntr)

			cmp_output = []
			for i in range(cmp_rows):
				try:
					line = Render.format2lines(cmp_lines[i], cmp_cols)[0]
				except:
					line = Render.fill_line(cmp_cols)
				cmp_output.append(line)

			if (orient == Orient.h):
				output = Render.zip(cmp_rows, output, cmp_output)
			else:
				output += cmp_output

		return output