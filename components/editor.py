from .component import Component
from .managers import Document


class LineNumbering(Component):

	def get_lines(self):
		from core import Render
		output = []
		for value in range(1, self.num+1):
			output.append(Render.fill_line(self.cols, str(value)))

		return output


class TextArea(Component):

	def __init__(self, filepath = None):
		self.doc = Document(filepath)
		self.call_stack_after = [
			self.configure_document
		] + self.doc.call_stack_after


	def configure_document(self):
		self.doc.indent_rows = self.indent_rows
		self.doc.indent_cols = self.indent_cols


	def get_lines(self):
		return self.doc.get_content()


class Editor(Component):
	cntr = []

	def __init__(self, filepath = None):
		self.line_num = LineNumbering()
		self.cntr.append({
			'cmp': self.line_num
		})

		self.text_area = TextArea(filepath)
		self.cntr.append({
			'cmp': self.text_area
		})

		self.call_stack_before = [
			self.configure_line_numbering
		]

	def configure_line_numbering(self):
		self.line_num.num = len(self.text_area.doc.content_lines)
		ln_cols = len(str(self.line_num.num)) + 1
		self.cntr[0]['cols'] = ln_cols