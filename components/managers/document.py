
from .content_manager import ContentManager as CM
from .cursor import Cursor

from pygments.lexers import guess_lexer, get_lexer_for_filename

class Document(Cursor, CM):
    def __init__(self, filepath):
        self.file = open(filepath)
        self.content = self.file.read(10260)

        self.content_lines = CM.split_lines(self.content)

        get_lexer_for_filename(self.file.name)
        self.lexer = guess_lexer(self.content)

        Cursor.__init__(self)


    def new_line(self):
        self.content_lines = CM.new_line(self.content_lines, (self.row, self.col))
        self.content = CM.format2content(self.content_lines)
        self.move_right()


    def indent(self):
        self.insert('\t')


    def insert(self, value):
        self.content_lines = CM.ins_in2(self.content_lines, value, (self.row, self.col))
        self.content = CM.format2content(self.content_lines)
        step = len(value)
        self.move_right(step)


    def insert_space(self, size:int = 1):
        value = ' ' * size
        self.insert(value)


    def delete_left(self, num:int = 1):
        fr0m = (self.row, self.col)
        to = CM.shift_left(self.content_lines, fr0m, num)
        self.content_lines = CM.del_context(self.content_lines, to, fr0m)
        self.content = CM.format2content(self.content_lines)
        self.row, self.col = to


    def delete_right(self, num:int = 1):
        fr0m = (self.row, self.col)
        to = CM.shift_right(self.content_lines, fr0m, num)
        self.content_lines = CM.del_context(self.content_lines, fr0m, to)
        self.content = CM.format2content(self.content_lines)


    def delete_left_word(self):
        lexed_content = CM.lex_content(self.content, self.lexer)
        lexed_content_lines = CM.lexed_content_split_lines(lexed_content)
        lexed_content_lines, (self.row, self.col) = CM.del_lword(lexed_content_lines, (self.row, self.col))
        self.content_lines = CM.lexed_content_format2content_lines(lexed_content_lines)
        self.content = CM.format2content(self.content_lines)


    def delete_right_word(self):
        lexed_content = CM.lex_content(self.content, self.lexer)
        lexed_content_lines = CM.lexed_content_split_lines(lexed_content)
        lexed_content_lines = CM.del_rword(lexed_content_lines, (self.row, self.col))
        self.content_lines = CM.lexed_content_format2content_lines(lexed_content_lines)
        self.content = CM.format2content(self.content_lines)


    def get_content(self):
        return self.content_lines