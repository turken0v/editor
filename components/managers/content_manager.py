from pygments import lex
from pygments.lexer import Lexer
from pygments.token import Token

class ContentManager:

    @staticmethod
    def lex_content(content: str, lexer):
        lexed_output = list(lex(content, lexer))

        lexed_content = []
        for _, value in lexed_output:
            lexed_content.append(value)

        return lexed_content


    @staticmethod
    def lexed_content_split_lines(lexed_content: list) -> list:
        lexed_content_lines = []
        line = []
        for value in lexed_content:
            if value == '\n':
                lexed_content_lines.append(line)
                line = []
            else:
                line.append(value)

        return lexed_content_lines


    @staticmethod
    def lexed_content_format2content_lines(lexed_content_lines: list) -> list:
        content_lines = []
        for lexed_line in lexed_content_lines:
            line = ''
            for value in lexed_line:
                line += value
            content_lines.append(line)

        return content_lines


    @staticmethod
    def format2content(lines: list) -> str:
        return '\n'.join(lines)


    @staticmethod
    def split_lines(content: str) -> list:
        return content.split('\n')


    @staticmethod
    def get_line(content_lines: list, row: int = 0, prev = False):
        while True:
            yield content_lines[row]

            if prev:
                row-=1
            else:
                row+=1


    @staticmethod
    def get_token(lexed_line: list):
        num = 0
        while lexed_line:
            try:
                yield lexed_line[num]
            except IndexError:
                break
            num += 1
        return StopIteration


    @staticmethod
    def get_ord_token_num(lexed_line: list, col: int = 0):
        num = 0
        token_gen = ContentManager.get_token(lexed_line)
        try:
            token_value = next(token_gen)
        except StopIteration:
            return None

        token_len = len(token_value)
        while col > token_len:
            try:
                token_value = next(token_gen)
            except StopIteration:
                break
            num += 1
            token_len += len(token_value)

        return num


    @staticmethod
    def shift_up(content_lines:list, fr0m:tuple, step:int = 1) -> tuple:
        row, col = fr0m

        if row < 1:
            return (0, 0)

        _row = row - step
        try:
            content_lines[_row][col]
        except:
            line_len = len(content_lines[_row])
            return (_row, line_len)

        return (_row, col)


    @staticmethod
    def shift_down(content_lines: list, fr0m: tuple, step: int) -> tuple:
        row, col = fr0m

        _row = row + step
        try:
            content_lines[_row]
        except:
            line_len = len(content_lines[row])
            return (row, line_len)

        try:
            content_lines[_row][col]
        except:
            return (_row, 0)

        return (_row, col)


    @staticmethod
    def shift_right(content_lines: list, fr0m: tuple, step: int) -> tuple:
        row, col = fr0m

        lines_num = len(content_lines)
        last_line_len = len(content_lines[lines_num-1])

        i = 0
        line_gen = ContentManager.get_line(content_lines, row)
        while True:
            line = next(line_gen)[col:]
            line_len = len(line)
            i += line_len
            if row == lines_num-1 and col + step - i + line_len > last_line_len-1:
                return (lines_num-1, last_line_len)
            if i >= step:
                break
            row += 1
            col = 0
            if line_len == 0:
                i += 1
                break

        col += step - i + line_len
        return (row, col)


    @staticmethod
    def shift_left(content_lines: list, fr0m: tuple, step: int) -> tuple:
        row, col = fr0m

        i = 0
        flag = True
        line_gen = ContentManager.get_line(content_lines, row, prev=True)
        while flag:
            line = next(line_gen)[:col]
            line_len = len(line)
            i += line_len if line_len > 0 else 1
            if row == 0 and i - step < 0: return (0, 0)
            if i >= step:
                if line_len == 0: flag = False
                else: break
            row -= 1
            col = None

        col = i - step
        return (row, col)


    @staticmethod
    def shift_right(content_lines: list, fr0m: tuple, step: int) -> tuple:
        row, col = fr0m

        lines_num = len(content_lines)
        last_line_len = len(content_lines[lines_num-1])

        i = 0
        line_gen = ContentManager.get_line(content_lines, row)
        while True:
            line = next(line_gen)[col:]
            line_len = len(line)
            i += line_len
            if row == lines_num-1 and col + step - i + line_len > last_line_len-1:
                return (lines_num-1, last_line_len)
            if i >= step:
                break
            row += 1
            col = 0
            if line_len == 0:
                i += 1
                break

        col += step - i + line_len
        return (row, col)


    @staticmethod
    def go2end_line(content_lines: list, row: int = 0) -> int:
        line_gen = ContentManager.get_line(content_lines, row)
        line_value = next(line_gen)
        return len(line_value)


    @staticmethod
    def ins_in2(content_lines: list, value: str, to: tuple) -> list:
        row, col = to

        line = content_lines[row][:col] + value + content_lines[row][col:]
        content_lines[row] = line

        return content_lines


    @staticmethod
    def new_line(content_lines: list, to: tuple) -> list:
        row, col = to

        content_lines.insert(row+1, content_lines[row][col:])
        content_lines[row] = content_lines[row][:col]

        return content_lines


    @staticmethod
    def del_context(content_lines: list, fr0m: tuple, to: tuple) -> list:
        row, col = fr0m
        _row, _col = to

        first = content_lines[:row]
        second = [content_lines[row][:col] + content_lines[_row][_col:]]
        third = content_lines[_row+1:]

        return first + second + third


    @staticmethod
    def get_list_content_len(list_content: list):
        return len(''.join(list_content))


    @staticmethod
    def get_overflow(before: list, pos):
        before_len = ContentManager.get_list_content_len(before)
        return pos - before_len


    @staticmethod
    def del_rword(lexed_content_lines: list, fr0m: tuple, _dir: int = None) -> list:
        row, col = fr0m

        lexed_line_gen = ContentManager.get_line(lexed_content_lines, row)
        lexed_line_value = next(lexed_line_gen)
        token_num = ContentManager.get_ord_token_num(lexed_line_value, col+1)

        balance = ContentManager.get_overflow(lexed_line_value[:token_num], col)
        token_value = lexed_line_value[token_num]
        lexed_line_value[token_num] = token_value[:balance]
        lexed_content_lines[row] = lexed_line_value

        return lexed_content_lines


    @staticmethod
    def del_lword(lexed_content_lines: list, fr0m: tuple):
        row, col = fr0m

        lexed_line_gen = ContentManager.get_line(lexed_content_lines, row)
        lexed_line_value = next(lexed_line_gen)
        token_num = ContentManager.get_ord_token_num(lexed_line_value, col)

        if token_num is None:
            del lexed_content_lines[row]
            _row = row - 1
            content_lines = ContentManager.lexed_content_format2content_lines([lexed_content_lines[_row]])
            _col = ContentManager.go2end_line(content_lines)
            return lexed_content_lines, (_row, _col)

        balance = ContentManager.get_overflow(lexed_line_value[:token_num], col)
        token_value = lexed_line_value[token_num]
        lexed_line_value[token_num] = token_value[balance:]
        lexed_content_lines[row] = lexed_line_value

        _col = col - balance

        return lexed_content_lines, (row, _col)


    @staticmethod
    def del_word(lexed_content_lines: list, fr0m: tuple) -> list:
        row, col = fr0m

        lexed_line_gen = ContentManager.get_line(lexed_content_lines, row)
        lexed_line_value = next(lexed_line_gen)
        token_num = ContentManager.get_ord_token_num(lexed_line_value, col)

        del lexed_line_value[token_num]
        lexed_content_lines[row] = lexed_line_value

        return lexed_content_lines