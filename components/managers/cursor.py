from .content_manager import ContentManager as CM
import core


class Cursor:
    def __init__(self):
        self.row = 0
        self.col = 0
        self.indent_rows = 0
        self.indent_cols = 0

        self.call_stack_after = [
            self._move
        ]


    def _move(self):
        seq = core.Terminal.move_yx(int(self.indent_rows + self.row), int(self.indent_cols + self.col))
        print(seq, end='', flush=True)


    def move_left(self, step: int = 1):
        self.row, self.col = CM.shift_left(self.content_lines, (self.row, self.col), step)
        self._move()


    def move_right(self, step: int = 1):
        self.row, self.col = CM.shift_right(self.content_lines, (self.row, self.col), step)
        self._move()


    def move_up(self, step: int = 1):
        self.row, self.col = CM.shift_up(self.content_lines, (self.row, self.col), step)
        self._move()


    def move_down(self, step: int = 1):
        self.row, self.col = CM.shift_down(self.content_lines, (self.row, self.col), step)
        self._move()