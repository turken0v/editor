class Component:
	content = ''

	def get_lines(self):
		return self.content.splitlines()